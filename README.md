# Interventies Beoordelen

Dit is een levend^[Een levend boek is een boek dat steeds bijgewerkt kan worden.] Open Access^[Dus nu, en altijd, gratis beschikbaar.] boek over het beoordelen van gedragsveranderingsinterventies (ook wel campagnes of programma's genoemd).

Dit boek is beschikbaar op https://a-bc.gitlab.io/interventies-beoordelen/.
